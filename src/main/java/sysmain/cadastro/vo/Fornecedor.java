package sysmain.cadastro.vo;

public class Fornecedor {
	private int codigoFornecedor;
	private String nome;
	private String endereco;
	private int cnpj;
	private String email;
	private int telefone;

	public Fornecedor(int codigoFornecedor, String nome, String endereco, int cnpj, String email, int telefone) {
		super();
		this.codigoFornecedor = codigoFornecedor;
		this.nome = nome;
		this.endereco = endereco;
		this.cnpj = cnpj;
		this.email = email;
		this.telefone = telefone;
	}

	public int getCodigoFornecedor() {
		return codigoFornecedor;
	}

	public void setCodigoFornecedor(int codigoFornecedor) {
		this.codigoFornecedor = codigoFornecedor;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public int getCnpj() {
		return cnpj;
	}

	public void setCnpj(int cnpj) {
		this.cnpj = cnpj;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getTelefone() {
		return telefone;
	}

	public void setTelefone(int telefone) {
		this.telefone = telefone;
	}

	public void manterFornecedor() {

	}

}
