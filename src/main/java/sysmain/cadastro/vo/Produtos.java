package sysmain.cadastro.vo;

public class Produtos {
	private int codigo;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Produtos(int codigo) {
		super();
		this.codigo = codigo;
	}

}
