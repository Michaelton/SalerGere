package sysmain.cadastro.vo;

import java.util.ArrayList;

public class Venda {
	private int codigoVenda;
	private ArrayList<Produtos> produtos = new ArrayList<Produtos>();
	private double valor;
	private String data;
	private String formadePagamento;

	public int getCodigoVenda() {
		return codigoVenda;
	}

	public void setCodigoVenda(int codigoVenda) {
		this.codigoVenda = codigoVenda;
	}

	public ArrayList<Produtos> getProdutos() {
		return produtos;
	}

	public void setProdutos(Produtos produtos) {
		this.produtos.add(produtos);
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getFormadePagamento() {
		return formadePagamento;
	}

	public void setFormadePagamento(String formadePagamento) {
		this.formadePagamento = formadePagamento;
	}

}
