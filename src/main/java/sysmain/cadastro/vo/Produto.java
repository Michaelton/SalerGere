package sysmain.cadastro.vo;

public class Produto {
	private int codigo;
	private String nome;
	private double preco;
	private String marca;
	private Fornecedor fornecedor;

	public Produto(int codigo, String nome, double preco, String marca, Fornecedor fornecedor) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.preco = preco;
		this.marca = marca;
		this.fornecedor = fornecedor;
	}

	public void manterProduto(int op) {

	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

}
