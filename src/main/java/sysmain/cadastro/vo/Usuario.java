package sysmain.cadastro.vo;

public class Usuario {
	private String tipoUsuario;
	private String login;
	private String senha;

	public Usuario(String tipoUsuario, String login, String senha) {
		super();
		this.tipoUsuario = tipoUsuario;
		this.login = login;
		this.senha = senha;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
