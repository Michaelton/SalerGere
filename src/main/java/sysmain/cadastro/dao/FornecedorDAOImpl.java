package sysmain.cadastro.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import sysmain.cadastro.vo.Fornecedor;
import sysmain.db.Conexao;

public class FornecedorDAOImpl implements FornecedorDAO {
	private Connection con = Conexao.conectar();
	private Scanner scan = new Scanner(System.in);
	private Fornecedor fornecedor = new Fornecedor(0, null, null, 0, null, 0);

	int op;

	@Override
	public void manterFornecedor() throws SQLException {
		System.out.println("Usuário Nível 1");
		System.out.println("1 - Cadastrar Fornecedor; 2 - Alterar Fornecedor; 3 - Excluir Fornecedor");
		System.out.print("Opção: ");
		op = Integer.parseInt(scan.nextLine());
		switch (op) {
		case 1:
			System.out.println("Cadastrar Fornecedor");
			try {
				do {
					System.out.print("Código do Fornecedor: ");
					fornecedor.setCodigoFornecedor(Integer.parseInt(scan.nextLine()));
					if ((fornecedor.getCodigoFornecedor()) == (consultarFornecedor(fornecedor.getCodigoFornecedor())
							.getCodigoFornecedor())) {
						System.out.println("Código do Fornecedor já Cadastrado");
					}
				} while (fornecedor.getCodigoFornecedor() == consultarFornecedor(fornecedor.getCodigoFornecedor())
						.getCodigoFornecedor());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			System.out.print("Nome: ");
			fornecedor.setNome(scan.nextLine());
			System.out.print("Endereço: ");
			fornecedor.setEndereco(scan.nextLine());
			System.out.print("CNPJ: ");
			fornecedor.setCnpj(Integer.parseInt(scan.nextLine()));
			System.out.print("E-Mail: ");
			fornecedor.setEmail(scan.nextLine());
			System.out.print("Telefone: ");
			fornecedor.setTelefone(Integer.parseInt(scan.nextLine()));

			cadastrarFornecedor(fornecedor);
			break;
		case 2:
			fornecedor = new Fornecedor(0, null, null, 0, null, 0);
			System.out.println("Usuário Nível 1");
			System.out.println("Alterar Fornecedor");
			try {
				do {
					System.out.print("Código do Fornecedor: ");
					fornecedor.setCodigoFornecedor(Integer.parseInt(scan.nextLine()));
					try {
						if (fornecedor.getCodigoFornecedor() != consultarFornecedor(fornecedor.getCodigoFornecedor())
								.getCodigoFornecedor()) {
							System.out.println("Código do Fornecedor não Existe");
						} else {
							fornecedor = consultarFornecedor(fornecedor.getCodigoFornecedor());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} while (fornecedor.getCodigoFornecedor() != consultarFornecedor(fornecedor.getCodigoFornecedor())
						.getCodigoFornecedor());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			System.out.println("Informações do Fornecedor");
			System.out.println("\tCódigo do Fornecedor: " + fornecedor.getCodigoFornecedor());
			System.out.println("\tNome: " + fornecedor.getNome());
			System.out.println("\tEdereço: " + fornecedor.getEndereco());
			System.out.println("\tCNPJ: " + fornecedor.getCnpj());
			System.out.println("\tE-Mail: " + fornecedor.getEmail());
			System.out.println("\tTelefone: " + fornecedor.getTelefone());
			System.out.print("Alterar Nome?( 1 - Sim; 2 não): ");
			op = Integer.parseInt(scan.nextLine());
			if (op == 1) {
				System.out.print("Novo Nome: ");
				fornecedor.setNome(scan.nextLine());
			}
			System.out.print("Alterar Endereço?( 1 - Sim; 2 não): ");
			op = Integer.parseInt(scan.nextLine());
			if (op == 1) {
				System.out.print("Novo Endereço: ");
				fornecedor.setEndereco(scan.nextLine());
			}
			System.out.print("Alterar CNPJ?( 1 - Sim; 2 não): ");
			op = Integer.parseInt(scan.nextLine());
			if (op == 1) {
				System.out.print("Novo CNPJ: ");
				fornecedor.setCnpj(Integer.parseInt(scan.nextLine()));
			}
			System.out.print("Alterar E-Mail?( 1 - Sim; 2 não): ");
			op = Integer.parseInt(scan.nextLine());
			if (op == 1) {
				System.out.print("Novo E-Mail: ");
				fornecedor.setEmail(scan.nextLine());
			}
			System.out.print("Alterar Telefone?( 1 - Sim; 2 não): ");
			op = Integer.parseInt(scan.nextLine());
			if (op == 1) {
				System.out.print("Novo Telefone: ");
				fornecedor.setTelefone(Integer.parseInt(scan.nextLine()));

				alterarFornecedor(fornecedor);
			}
			break;
		case 3:
			Fornecedor fornecedorExcluir = new Fornecedor(0, null, null, 0, null, 0);
			System.out.println("Usuário Nível 1");
			System.out.println("Excluir Fornecedor");
			System.out.print("Código do Fornecedor: ");
			fornecedor.setCodigoFornecedor(Integer.parseInt(scan.nextLine()));
			try {
				if (fornecedor.getCodigoFornecedor() == consultarFornecedor(fornecedor.getCodigoFornecedor())
						.getCodigoFornecedor()) {
					fornecedorExcluir = consultarFornecedor(fornecedor.getCodigoFornecedor());
					System.out.println("Informações do Fornecedor");
					System.out.println("\tCódigo do Fornecedor: " + fornecedorExcluir.getCodigoFornecedor());
					System.out.println("\tNome: " + fornecedorExcluir.getNome());
					System.out.println("\tEdereço: " + fornecedorExcluir.getEndereco());
					System.out.println("\tCNPJ: " + fornecedorExcluir.getCnpj());
					System.out.println("\tE-Mail: " + fornecedorExcluir.getEmail());
					System.out.println("\tTelefone: " + fornecedorExcluir.getTelefone());
					System.out.print("Confirmar Exclusão(1 - Sim; 2 - Não): ");
					op = Integer.parseInt(scan.nextLine());
					if (op == 1) {
						excluirFornecedor(fornecedorExcluir.getCodigoFornecedor());
					}
				} else {
					System.out.println("Código do Fornecedor não Existe");
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			break;
		default:
			break;
		}

	}

	@Override
	public Fornecedor consultarFornecedor(int codigo) throws SQLException {
		Fornecedor fornecedorConsultar = new Fornecedor(0, null, null, 0, null, 0);
		String sql = "select * from fornecedor where codigoFornecedor = ?";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setInt(1, codigo);

		ResultSet rs = sqlComand.executeQuery();
		while (rs.next()) {
			fornecedorConsultar = new Fornecedor(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4),
					rs.getString(5), rs.getInt(6));
		}
		return fornecedorConsultar;
	}

	@Override
	public void cadastrarFornecedor(Fornecedor fornecedor) throws SQLException {
		String sql = "insert into fornecedor values(?,?,?,?,?,?)";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setInt(1, fornecedor.getCodigoFornecedor());
		sqlComand.setString(2, fornecedor.getNome());
		sqlComand.setString(3, fornecedor.getEndereco());
		sqlComand.setInt(4, fornecedor.getCnpj());
		sqlComand.setString(5, fornecedor.getEmail());
		sqlComand.setInt(6, fornecedor.getTelefone());
		sqlComand.execute();
		System.out.println("Fornecedor Cadastrado com Sucesso");

	}

	@Override
	public void alterarFornecedor(Fornecedor fornecedor) throws SQLException {
		String sql = "update fornecedor set nome = ?, endereco = ?, CNPJ = ?, email = ?, telefone = ? where codigoFornecedor = ?";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setString(1, fornecedor.getNome());
		sqlComand.setString(2, fornecedor.getEndereco());
		sqlComand.setInt(3, fornecedor.getCnpj());
		sqlComand.setString(4, fornecedor.getEmail());
		sqlComand.setInt(5, fornecedor.getTelefone());
		sqlComand.setInt(6, fornecedor.getCodigoFornecedor());
		sqlComand.execute();
		System.out.println("Alteração Realizada com Sucesso");

	}

	@Override
	public void excluirFornecedor(int codigo) throws SQLException {
		String sql = "delete from fornecedor where codigoFornecedor = ?";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setInt(1, codigo);
		sqlComand.execute();
		System.out.println("Fornecedor Excluido com Sucesso");

	}

}
