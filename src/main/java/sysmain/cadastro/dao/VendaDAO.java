package sysmain.cadastro.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import sysmain.cadastro.vo.Produtos;
import sysmain.cadastro.vo.Venda;

public interface VendaDAO {

	void manterVenda() throws SQLException;

	Venda consultarVenda(int codigoVenda) throws SQLException;

	void cadastrarVenda(Venda venda) throws SQLException;

	void alterarVenda(Venda venda, ArrayList<Produtos> listremove, ArrayList<Produtos> listADD) throws SQLException;

	void excluirVenda(int codigoVenda) throws SQLException;

}
