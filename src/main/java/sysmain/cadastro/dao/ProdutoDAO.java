package sysmain.cadastro.dao;

import java.sql.SQLException;

import sysmain.cadastro.vo.Produto;

public interface ProdutoDAO {
	void manterProduto() throws SQLException;

	Produto consultarProduto(int codigo) throws SQLException;

	void cadastrarProduto(Produto produto) throws SQLException;

	void alterarProduto(Produto produto) throws SQLException;

	void excluirProduto(int codigo) throws SQLException;

}
