package sysmain.cadastro.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import sysmain.cadastro.vo.Produto;
import sysmain.cadastro.vo.Produtos;
import sysmain.cadastro.vo.Venda;
import sysmain.db.Conexao;

public class VendaDAOImpl implements VendaDAO {
	private Connection con = Conexao.conectar();
	private Scanner scan = new Scanner(System.in);
	private Venda venda = new Venda();

	private int op;
	private double totalValor;

	@Override
	public void manterVenda() throws SQLException {
		System.out.println("Usuário Nível 1");
		System.out.println("1 - Cadastrar Venda; 2 - Alterar Venda; 3 - Excluir Venda");
		System.out.print("Opção: ");
		op = Integer.parseInt(scan.nextLine());
		switch (op) {
		case 1:
			System.out.println("Cadastrar Venda");
			try {
				do {
					System.out.print("Código do Venda: ");
					venda.setCodigoVenda(Integer.parseInt(scan.nextLine()));
					if ((venda.getCodigoVenda()) == (consultarVenda(venda.getCodigoVenda()).getCodigoVenda())) {
						System.out.println("Código da Venda já Cadastrado");
					}
				} while (venda.getCodigoVenda() == consultarVenda(venda.getCodigoVenda()).getCodigoVenda());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}

			do {
				Produtos produtos = new Produtos(0);
				System.out.print("Código do Produto: ");
				produtos.setCodigo(Integer.parseInt(scan.nextLine()));
				venda.setProdutos(produtos);
				System.out.print("Adcionar outro Produto? 1 - Sim; 2 - Não: ");
				op = Integer.parseInt(scan.nextLine());
			} while (op != 2);

			System.out.print("Valor: ");
			totalValor = 0;
			for (Produtos lista : venda.getProdutos()) {
				ProdutoDAOImpl produtoDOAImpl = new ProdutoDAOImpl();
				totalValor += produtoDOAImpl.consultarProduto(lista.getCodigo()).getPreco();
			}
			venda.setValor(totalValor);
			System.out.print("Data(AAAA-MM-DD): ");
			venda.setData(scan.nextLine());
			System.out.println("Escolha a Forma de Pagamento: 1 - Dinheiro; 2 - Crédito; 3 - Débito ");
			System.out.print("Opção: ");
			op = Integer.parseInt(scan.nextLine());
			switch (op) {
			case 1:
				venda.setFormadePagamento("Dinheiro");
				break;
			case 2:
				venda.setFormadePagamento("Crédito");
				break;
			case 3:
				venda.setFormadePagamento("Débito");
				break;
			default:
				break;
			}

			cadastrarVenda(venda);
			break;
		case 2:
			venda = new Venda();
			ArrayList<Produtos> listRemove = new ArrayList<Produtos>();
			ArrayList<Produtos> listADD = new ArrayList<Produtos>();
			System.out.println("Usuário Nível 1");
			System.out.println("Alterar Venda");
			try {
				do {
					System.out.print("Código da Venda: ");
					venda.setCodigoVenda(Integer.parseInt(scan.nextLine()));
					try {
						if (venda.getCodigoVenda() != consultarVenda(venda.getCodigoVenda()).getCodigoVenda()) {
							System.out.println("Código da Venda não Existe");
						} else {
							venda = consultarVenda(venda.getCodigoVenda());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} while (venda.getCodigoVenda() != consultarVenda(venda.getCodigoVenda()).getCodigoVenda());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			System.out.println("Informações da Venda");
			System.out.println("\tCódigo da Venda: " + venda.getCodigoVenda());

			System.out.println("Lista de Produtos");

			for (Produtos lista : venda.getProdutos()) {
				Produto produto = new Produto(0, null, 0, null, null);
				ProdutoDAOImpl produtoDOAImpl = new ProdutoDAOImpl();
				produto = produtoDOAImpl.consultarProduto(lista.getCodigo());

				System.out.println("\nInformações do Produto");
				System.out.println("\tCódigo: " + produto.getCodigo());
				System.out.println("\tNome: " + produto.getNome());
				System.out.println("\tPreço: " + produto.getPreco());
				System.out.println("\tMarca: " + produto.getMarca());
				System.out.println("\tCódigo do Fornecedor: " + produto.getFornecedor().getCodigoFornecedor());

			}

			System.out.println("\nValor: " + venda.getValor());
			System.out.println("Data: " + venda.getData());
			System.out.println("Forma de Pagamento: " + venda.getFormadePagamento());

			totalValor = venda.getValor();
			for (Produtos lista : venda.getProdutos()) {
				Produto produto = new Produto(0, null, 0, null, null);
				ProdutoDAOImpl produtoDOAImpl = new ProdutoDAOImpl();
				produto = produtoDOAImpl.consultarProduto(lista.getCodigo());

				System.out.println("\nInformações do Produto");
				System.out.println("\tCódigo: " + produto.getCodigo());
				System.out.println("\tNome: " + produto.getNome());
				System.out.println("\tPreço: " + produto.getPreco());
				System.out.println("\tMarca: " + produto.getMarca());
				System.out.println("\tCódigo do Fornecedor: " + produto.getFornecedor().getCodigoFornecedor());

				System.out.println("Remover Este Produto da Lisat? 1 - Sim; 2 - Não: ");
				op = Integer.parseInt(scan.nextLine());
				if (op == 1) {
					listRemove.add(lista);
					totalValor -= produto.getPreco();
				}
			}

			do {
				Produtos produtos = new Produtos(0);
				Produto produto = new Produto(0, null, 0, null, null);
				ProdutoDAOImpl produtoDOAImpl = new ProdutoDAOImpl();
				System.out.println("Adcionar outro Produto? 1 - Sim; 2 - Não: ");
				op = Integer.parseInt(scan.nextLine());
				if (op == 1) {
					System.out.print("Código do Produto: ");
					produtos.setCodigo(Integer.parseInt(scan.nextLine()));
					listADD.add(produtos);
					produto = produtoDOAImpl.consultarProduto(produtos.getCodigo());
					totalValor += produto.getPreco();
				}
			} while (op == 1);
			venda.setValor(totalValor);
			System.out.println("Novo Valor: " + venda.getValor());
			System.out.print("Alterar Data?( 1 - Sim; 2 não): ");
			op = Integer.parseInt(scan.nextLine());
			if (op == 1) {
				System.out.print("Nova Data(AAAA-MM-DD): ");
				venda.setData(scan.nextLine());
			}
			System.out.print("Alterar Forma de Pagamento?( 1 - Sim; 2 não): ");
			op = Integer.parseInt(scan.nextLine());
			if (op == 1) {
				System.out.println("Escolha a Nova Forma de Pagamento: 1 - Dinheiro; 2 - Crédito; 3 - Débito ");
				System.out.print("Opção: ");
				op = Integer.parseInt(scan.nextLine());
				switch (op) {
				case 1:
					venda.setFormadePagamento("Dinheiro");
					break;
				case 2:
					venda.setFormadePagamento("Crédito");
					break;
				case 3:
					venda.setFormadePagamento("Débito");
					break;
				default:
					break;
				}
			}

			alterarVenda(venda, listRemove, listADD);

			break;
		case 3:
			Venda vendaExcluir = new Venda();
			System.out.println("Usuário Nível 1");
			System.out.println("Excluir Venda");
			System.out.print("Código da Venda: ");
			venda.setCodigoVenda(Integer.parseInt(scan.nextLine()));
			try {
				if (venda.getCodigoVenda() == consultarVenda(venda.getCodigoVenda()).getCodigoVenda()) {
					vendaExcluir = consultarVenda(venda.getCodigoVenda());
					System.out.println("Informações da Venda");
					System.out.println("\tCódigo da Venda: " + vendaExcluir.getCodigoVenda());

					System.out.println("Lista de Produtos");

					for (Produtos lista : vendaExcluir.getProdutos()) {
						Produto produto = new Produto(0, null, 0, null, null);
						ProdutoDAOImpl produtoDOAImpl = new ProdutoDAOImpl();
						produto = produtoDOAImpl.consultarProduto(lista.getCodigo());

						System.out.println("\nInformações do Produto");
						System.out.println("\tCódigo: " + produto.getCodigo());
						System.out.println("\tNome: " + produto.getNome());
						System.out.println("\tEdereço: " + produto.getPreco());
						System.out.println("\tMarca: " + produto.getMarca());
						System.out.println("\tCódigo do Fornecedor: " + produto.getFornecedor().getCodigoFornecedor());

					}

					System.out.println("\tValor: " + vendaExcluir.getValor());
					System.out.println("\tData: " + vendaExcluir.getData());
					System.out.println("\tForma de Pagamento: " + vendaExcluir.getFormadePagamento());
					System.out.print("Confirmar Exclusão(1 - Sim; 2 - Não): ");
					op = Integer.parseInt(scan.nextLine());
					if (op == 1) {
						excluirVenda(vendaExcluir.getCodigoVenda());
					}
				} else {
					System.out.println("Código da Venda não Existe");
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			break;
		default:
			break;
		}

	}

	@Override
	public Venda consultarVenda(int codigoVenda) throws SQLException {

		Venda vendaConsultar = new Venda();
		String sql = "select * from venda where codigoVenda = ?";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setInt(1, codigoVenda);

		ResultSet rs = sqlComand.executeQuery();
		while (rs.next()) {
			vendaConsultar.setCodigoVenda(rs.getInt(1));
			vendaConsultar.setValor(rs.getDouble(2));
			vendaConsultar.setData(rs.getString(3));
			vendaConsultar.setFormadePagamento(rs.getString(4));

		}

		sql = "select * from venda_produto where codigoVenda = ?";
		sqlComand = con.prepareStatement(sql);
		sqlComand.setInt(1, codigoVenda);

		rs = sqlComand.executeQuery();
		while (rs.next()) {
			Produtos produtosConsultar = new Produtos(0);
			produtosConsultar.setCodigo(rs.getInt(1));
			vendaConsultar.setProdutos(produtosConsultar);
		}

		return vendaConsultar;
	}

	@Override
	public void cadastrarVenda(Venda venda) throws SQLException {

		String sql = "insert into venda values(?,?,?,?)";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setInt(1, venda.getCodigoVenda());
		sqlComand.setDouble(2, venda.getValor());
		sqlComand.setString(3, venda.getData());
		sqlComand.setString(4, venda.getFormadePagamento());
		sqlComand.execute();

		for (Produtos list : venda.getProdutos()) {
			sql = "insert into venda_produto values(?,?)";
			con = Conexao.conectar();
			sqlComand = con.prepareStatement(sql);
			sqlComand.setInt(1, list.getCodigo());
			sqlComand.setInt(2, venda.getCodigoVenda());
			sqlComand.execute();
		}

		System.out.println("Venda Cadastrada com Sucesso");

	}

	@Override
	public void alterarVenda(Venda venda, ArrayList<Produtos> listremove, ArrayList<Produtos> listADD)
			throws SQLException {
		String sql;

		sql = "update venda set valor = ?, data = ?, formaPagamento = ? where codigoVenda = ?";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setDouble(1, venda.getValor());
		sqlComand.setString(2, venda.getData());
		sqlComand.setString(3, venda.getFormadePagamento());
		sqlComand.setInt(4, venda.getCodigoVenda());
		sqlComand.execute();

		if (listremove.size() > 0) {
			for (Produtos list : listremove) {
				sql = "delete from venda_produto where codigoVenda = ?";
				con = Conexao.conectar();
				sqlComand = con.prepareStatement(sql);
				sqlComand.setInt(1, list.getCodigo());
				sqlComand.execute();
			}
		}

		if (listADD.size() > 0) {
			for (Produtos list : listADD) {
				sql = "insert into venda_produto values(?,?)";
				con = Conexao.conectar();
				sqlComand = con.prepareStatement(sql);
				sqlComand.setInt(1, list.getCodigo());
				sqlComand.setInt(2, venda.getCodigoVenda());
				sqlComand.execute();
			}
		}

		System.out.println("Alteração Realizada com Sucesso");

	}

	@Override
	public void excluirVenda(int codigoVenda) throws SQLException {
		String sql;

		sql = "delete from venda_produto where codigoVenda = ?";

		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setInt(1, codigoVenda);
		sqlComand.execute();

		sql = "delete from venda where codigoVenda = ?";
		sqlComand = con.prepareStatement(sql);
		sqlComand.setInt(1, codigoVenda);
		sqlComand.execute();

		System.out.println("Venda Excluida com Sucesso");

	}

}
