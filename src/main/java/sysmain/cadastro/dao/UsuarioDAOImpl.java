package sysmain.cadastro.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import sysmain.cadastro.vo.Usuario;
import sysmain.db.Conexao;

public class UsuarioDAOImpl implements UsuarioDAO {
	private Connection con = Conexao.conectar();
	private Scanner scan = new Scanner(System.in);
	private Usuario user = new Usuario(null, null, null);
	private Usuario userConsultar = new Usuario(null, null, null);
	private Usuario userExcluir = new Usuario(null, null, null);

	private int op;

	@Override
	public String fazerLogin(String login) throws SQLException {
		String senha = null;
		String sql = "select * from usuario where login = ?";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setString(1, login);

		ResultSet rs = sqlComand.executeQuery();
		while (rs.next()) {
			user = new Usuario(rs.getString("tipousuario"), rs.getString("login"), rs.getString("senha"));
		}
		if (login.equals(user.getLogin())) {
			System.out.print("Senha: ");
			senha = scan.nextLine();
			if (senha.equals(user.getSenha())) {
			} else {
				System.out.println("Senha Informada está Errada, Tente Novamente ou Contate o Administrador");
				user.setTipoUsuario("No User");
			}
		} else {
			System.out.println("Usuário não Cadastrado, Tente Novamente ou Contate o Administrador");
			user.setTipoUsuario("No User");
		}

		return user.getTipoUsuario();
	}

	@Override
	public void manterUsuario() throws SQLException {
		System.out.println("Administrador");
		System.out.println("1 - Cadastrar Usuário; 2 - Excluir Usuário");
		System.out.print("Opção: ");
		op = Integer.parseInt(scan.nextLine());
		switch (op) {
		case 1:
			System.out.println("Cadastrar Usuário");
			System.out.print("Tipo de Usuário(1 - Usuário Nível 1;): ");
			op = Integer.parseInt(scan.nextLine());
			switch (op) {
			case 1:
				user.setTipoUsuario("Usuário Nível 1");
				break;

			default:
				break;
			}
			do {
				System.out.print("Login: ");
				user.setLogin(scan.nextLine());
				if (user.getLogin().equals(consultarUsuario(user.getLogin()).getLogin())) {
					System.out.println("Usuário já Cadastrado");
				}
			} while (user.getLogin().equals(consultarUsuario(user.getLogin()).getLogin()));
			System.out.print("Senha: ");
			user.setSenha(scan.nextLine());
			cadastrarUsuario(user);
			break;
		case 2:
			System.out.println("Administrador");
			System.out.println("Excluir Usuário");
			System.out.print("Login: ");
			user.setLogin(scan.nextLine());
			if (user.getLogin().equals(consultarUsuario(user.getLogin()).getLogin())) {
				userExcluir = consultarUsuario(user.getLogin());
				System.out.println("Informações do Usuário");
				System.out.println("\tLogin: " + userExcluir.getLogin());
				System.out.println("\tTipo de Usuário: " + userExcluir.getTipoUsuario());
				System.out.print("Confirmar Exclusão(1 - Sim; 2 - Não): ");
				op = Integer.parseInt(scan.nextLine());
				if (op == 1) {
					excluirUsuario(userExcluir.getLogin());
				}
			} else {
				System.out.println("Login de Usuário não Existe");
			}
			break;
		default:
			break;
		}

	}

	@Override
	public Usuario consultarUsuario(String login) throws SQLException {
		String sql = "select * from usuario where login = ?";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setString(1, login);

		ResultSet rs = sqlComand.executeQuery();
		while (rs.next()) {
			userConsultar = new Usuario(rs.getString("tipousuario"), rs.getString("login"), rs.getString("senha"));
		}
		return userConsultar;
	}

	@Override
	public void cadastrarUsuario(Usuario usuario) throws SQLException {
		String sql = "insert into usuario values(?,?,?)";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setString(1, usuario.getTipoUsuario());
		sqlComand.setString(2, usuario.getLogin());
		sqlComand.setString(3, usuario.getSenha());
		sqlComand.execute();
		System.out.println("Usuário Cadastrado com Sucesso");

	}

	@Override
	public void excluirUsuario(String login) throws SQLException {
		String sql = "delete from usuario where login = ?";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setString(1, login);
		sqlComand.execute();
		System.out.println("Usuário Excluido com Sucesso");
	}

}
