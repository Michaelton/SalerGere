package sysmain.cadastro.dao;

import java.sql.SQLException;

import sysmain.cadastro.vo.Usuario;

public interface UsuarioDAO {
	String fazerLogin(String login) throws SQLException;

	void manterUsuario() throws SQLException;

	Usuario consultarUsuario(String login) throws SQLException;

	void cadastrarUsuario(Usuario usuario) throws SQLException;

	void excluirUsuario(String login) throws SQLException;
}
