package sysmain.cadastro.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import sysmain.cadastro.vo.Fornecedor;
import sysmain.cadastro.vo.Produto;
import sysmain.db.Conexao;

public class ProdutoDAOImpl implements ProdutoDAO {
	private Connection con = Conexao.conectar();
	private Scanner scan = new Scanner(System.in);
	private Produto produto = new Produto(0, null, 0, null, null);
	private Fornecedor fornecedor = new Fornecedor(0, null, null, 0, null, 0);

	private int op;

	@Override
	public void manterProduto() throws SQLException {
		fornecedor = new Fornecedor(0, null, null, 0, null, 0);
		System.out.println("Usuário Nível 1");
		System.out.println("1 - Cadastrar Produto; 2 - Alterar Produto; 3 - Excluir Produto");
		System.out.print("Opção: ");
		op = Integer.parseInt(scan.nextLine());
		switch (op) {
		case 1:
			System.out.println("Cadastrar Produto");
			try {
				do {
					System.out.print("Código do Produto: ");
					produto.setCodigo(Integer.parseInt(scan.nextLine()));
					if ((produto.getCodigo()) == (consultarProduto(produto.getCodigo()).getCodigo())) {
						System.out.println("Código do Produto já Cadastrado");
					}
				} while ((produto.getCodigo()) == (consultarProduto(produto.getCodigo()).getCodigo()));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			System.out.print("Nome: ");
			produto.setNome(scan.nextLine());
			System.out.print("Preço: ");
			produto.setPreco(Double.parseDouble(scan.nextLine()));
			System.out.print("Marca: ");
			produto.setMarca(scan.nextLine());
			System.out.print("Código do Fornecedor: ");
			fornecedor.setCodigoFornecedor(Integer.parseInt(scan.nextLine()));
			produto.setFornecedor(fornecedor);

			cadastrarProduto(produto);
			break;
		case 2:
			fornecedor = new Fornecedor(0, null, null, 0, null, 0);
			produto = new Produto(0, null, 0, null, fornecedor);

			System.out.println("Usuário Nível 1");
			System.out.println("Alterar Produto");
			try {
				do {
					System.out.print("Código do Produto: ");
					produto.setCodigo(Integer.parseInt(scan.nextLine()));
					try {
						if (produto.getCodigo() != consultarProduto(produto.getCodigo()).getCodigo()) {
							System.out.println("Código do Produto não Existe");
						} else {
							produto = consultarProduto(produto.getCodigo());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} while (produto.getCodigo() != consultarProduto(produto.getCodigo()).getCodigo());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			System.out.println("Informações do Produto");
			System.out.println("\tCódigo: " + produto.getCodigo());
			System.out.println("\tNome: " + produto.getNome());
			System.out.println("\tPreço: " + produto.getPreco());
			System.out.println("\tMarca: " + produto.getMarca());
			System.out.println("\tCódigo do Fornecedor: " + produto.getFornecedor().getCodigoFornecedor());

			System.out.print("Alterar Nome?( 1 - Sim; 2 não): ");
			op = Integer.parseInt(scan.nextLine());
			if (op == 1) {
				System.out.print("Novo Nome: ");
				produto.setNome(scan.nextLine());
			}
			System.out.print("Alterar Preço?( 1 - Sim; 2 não): ");
			op = Integer.parseInt(scan.nextLine());
			if (op == 1) {
				System.out.print("Novo Preço: ");
				produto.setPreco(Double.parseDouble(scan.nextLine()));
			}
			System.out.print("Alterar Marca?( 1 - Sim; 2 não): ");
			op = Integer.parseInt(scan.nextLine());
			if (op == 1) {
				System.out.print("Novo marca: ");
				produto.setMarca(scan.nextLine());
			}
			System.out.print("Alterar Código do Fornecedor?( 1 - Sim; 2 não): ");
			op = Integer.parseInt(scan.nextLine());
			if (op == 1) {
				System.out.print("Novo Código do Fornecedor: ");
				fornecedor.setCodigoFornecedor(Integer.parseInt(scan.nextLine()));
				produto.setFornecedor(fornecedor);
			}

			alterarProduto(produto);

			break;
		case 3:
			fornecedor = new Fornecedor(0, null, null, 0, null, 0);
			Produto produtoExcluir = new Produto(0, null, 0, null, fornecedor);
			System.out.println("Usuário Nível 1");
			System.out.println("Excluir Produto");
			System.out.print("Código: ");
			produtoExcluir.setCodigo(Integer.parseInt(scan.nextLine()));
			try {
				if ((produtoExcluir.getCodigo()) == (consultarProduto(produtoExcluir.getCodigo()).getCodigo())) {
					produtoExcluir = consultarProduto(produtoExcluir.getCodigo());
					System.out.println("Informações do Produto");
					System.out.println("\tCódigo: " + produtoExcluir.getCodigo());
					System.out.println("\tNome: " + produtoExcluir.getNome());
					System.out.println("\tPreço: " + produtoExcluir.getPreco());
					System.out.println("\tMarca: " + produtoExcluir.getMarca());
					System.out.println("\tFornecedor: " + produtoExcluir.getFornecedor().getCodigoFornecedor());
					System.out.print("Confirmar Exclusão(1 - Sim; 2 - Não): ");
					op = Integer.parseInt(scan.nextLine());
					if (op == 1) {
						excluirProduto(produtoExcluir.getCodigo());
					}
				} else {
					System.out.println("Código do Produto não Existe");
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			break;
		default:
			break;
		}

	}

	@Override
	public Produto consultarProduto(int codigo) throws SQLException {
		fornecedor = new Fornecedor(0, null, null, 0, null, 0);
		Produto produtoConsultar = new Produto(0, null, 0, null, null);
		String sql = "select * from produto where codigo = ?";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setInt(1, codigo);

		ResultSet rs = sqlComand.executeQuery();
		while (rs.next()) {
			produtoConsultar.setCodigo(rs.getInt(1));
			produtoConsultar.setNome(rs.getString(2));
			produtoConsultar.setPreco(rs.getDouble(3));
			produtoConsultar.setMarca(rs.getString(4));
			fornecedor.setCodigoFornecedor(rs.getInt(5));
			produtoConsultar.setFornecedor(fornecedor);
		}
		return produtoConsultar;
	}

	@Override
	public void cadastrarProduto(Produto produto)
			throws SQLException {
		String sql = "insert into produto values(?,?,?,?,?)";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setInt(1, produto.getCodigo());
		sqlComand.setString(2, produto.getNome());
		sqlComand.setDouble(3, produto.getPreco());
		sqlComand.setString(4, produto.getMarca());
		sqlComand.setInt(5, produto.getFornecedor().getCodigoFornecedor());
		sqlComand.execute();
		System.out.println("Produto Cadastrado com Sucesso");

	}

	@Override
	public void alterarProduto(Produto produto)
			throws SQLException {
		String sql = "update produto set nome = ?, preco = ?, marca = ?, codigoFornecedor = ? where codigo = ?";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setString(1, produto.getNome());
		sqlComand.setDouble(2, produto.getPreco());
		sqlComand.setString(3, produto.getMarca());
		sqlComand.setInt(4, produto.getFornecedor().getCodigoFornecedor());
		sqlComand.setInt(5, produto.getCodigo());
		sqlComand.execute();
		System.out.println("Alteração Realizada com Sucesso");

	}

	@Override
	public void excluirProduto(int codigo) throws SQLException {
		String sql = "delete from produto where codigo = ?";
		con = Conexao.conectar();
		PreparedStatement sqlComand = con.prepareStatement(sql);
		sqlComand.setInt(1, codigo);
		sqlComand.execute();
		System.out.println("Produto Excluido com Sucesso");

	}

}
