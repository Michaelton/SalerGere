package sysmain.cadastro.dao;

import java.sql.SQLException;

import sysmain.cadastro.vo.Fornecedor;

public interface FornecedorDAO {
	void manterFornecedor() throws SQLException;

	Fornecedor consultarFornecedor(int codigo) throws SQLException;

	void cadastrarFornecedor(Fornecedor fornecedor) throws SQLException;

	void alterarFornecedor(Fornecedor fornecedor) throws SQLException;

	void excluirFornecedor(int codigo) throws SQLException;

}
