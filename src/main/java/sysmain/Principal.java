package sysmain;

import java.sql.SQLException;
import java.util.Scanner;

import sysmain.cadastro.dao.FornecedorDAOImpl;
import sysmain.cadastro.dao.ProdutoDAOImpl;
import sysmain.cadastro.dao.UsuarioDAOImpl;
import sysmain.cadastro.dao.VendaDAOImpl;
import sysmain.cadastro.vo.Fornecedor;
import sysmain.cadastro.vo.Produto;
import sysmain.cadastro.vo.Produtos;
import sysmain.cadastro.vo.Usuario;
import sysmain.cadastro.vo.Venda;

public class Principal {

	public static void main(String[] args) throws SQLException {
		Usuario usuario = new Usuario(null, null, null);
		UsuarioDAOImpl user = new UsuarioDAOImpl();
		FornecedorDAOImpl fornecedorDAOImpl = new FornecedorDAOImpl();
		Produto produto = new Produto(0, null, 0, null, null);
		ProdutoDAOImpl produtoDAOImpl = new ProdutoDAOImpl();
		Venda venda = new Venda();
		VendaDAOImpl vendaDAOImpl = new VendaDAOImpl();

		int op = 0;
		String logoff = null;
		String exit = null;

		Scanner scan = new Scanner(System.in);

		do {
			do {
				System.out.println("\n\n\tSalerGere");
				do {
					System.out.print("Login: ");
					usuario.setTipoUsuario(user.fazerLogin(scan.nextLine()));
				} while (usuario.getTipoUsuario().equals("No User"));
				System.out.println("\tBem Vindo");
				if (usuario.getTipoUsuario().equals("admin")) {

					do {
						System.out.println("\nAdministrador");
						System.out.println("1 - Manter Usuário; 2 - Consultar Usuário; 3 - Logoff; 4 - Sair");
						System.out.print("Opção: ");
						op = Integer.parseInt(scan.nextLine());
						switch (op) {
						case 1:
							System.out.println("\n");
							user.manterUsuario();
							break;
						case 2:
							String consulta;
							System.out.println("Consultar Usuário");
							System.out.print("Login: ");
							consulta = scan.nextLine();
							Usuario userConsulta = new Usuario(null, null, null);
							if (consulta.equals(user.consultarUsuario(consulta).getLogin())) {
								userConsulta = user.consultarUsuario(consulta);
								System.out.println("Informações do Usuário:");
								System.out.println("\tTipo de Usuário: " + userConsulta.getTipoUsuario());
								System.out.println("\tLogin: " + userConsulta.getLogin());
							} else {
								System.out.println("Login de Usuário não Existe");
							}
							break;
						default:
							break;
						}
					} while (op != 3 & op != 4);
					logoff = "logoff";
					System.out.println("Bye");
					if (op == 4) {
						exit = "exit";
					}

				}
				if (usuario.getTipoUsuario().equals("Usuário Nível 1")) {
					do {
						System.out.println("\nUsuário Nível 1");
						System.out.println("1 - Manter Fornecedor; 	  2 - Manter Produto;    3 - Manter Venda; ");
						System.out.println("4 - Consultar Fornecedor; 5 - Consultar Produto; 6 - Consultar Venda; ");
						System.out.println("7 - Logoff");
						System.out.print("Opção: ");
						op = Integer.parseInt(scan.nextLine());
						switch (op) {
						case 1:
							System.out.println("\n");
							fornecedorDAOImpl.manterFornecedor();
							break;
						case 2:
							System.out.println("\n");
							produtoDAOImpl.manterProduto();
							break;
						case 3:
							System.out.println("\n");
							vendaDAOImpl.manterVenda();
							;
							break;
						case 4:
							int consulta = 0;
							System.out.println("Consultar Fornecedor");
							System.out.print("Código do Fornecedor: ");
							consulta = Integer.parseInt(scan.nextLine());
							Fornecedor fornecedorConsulta = new Fornecedor(0, null, null, 0, null, 0);
							if (consulta == fornecedorDAOImpl.consultarFornecedor(consulta).getCodigoFornecedor()) {
								fornecedorConsulta = fornecedorDAOImpl.consultarFornecedor(consulta);
								System.out.println("Informações do Fornecedor");
								System.out
										.println("\tCódigo do Fornecedor: " + fornecedorConsulta.getCodigoFornecedor());
								System.out.println("\tNome: " + fornecedorConsulta.getNome());
								System.out.println("\tEdereço: " + fornecedorConsulta.getEndereco());
								System.out.println("\tCNPJ: " + fornecedorConsulta.getCnpj());
								System.out.println("\tE-Mail: " + fornecedorConsulta.getEmail());
								System.out.println("\tTelefone: " + fornecedorConsulta.getTelefone());
							} else {
								System.out.println("Código do Fornecedor não Existe");
							}
							break;
						case 5:
							consulta = 0;
							System.out.println("Consultar Produto");
							System.out.print("Código: ");
							consulta = Integer.parseInt(scan.nextLine());
							Produto produtoConsulta = null;
							if (consulta == produtoDAOImpl.consultarProduto(consulta).getCodigo()) {
								produtoConsulta = produtoDAOImpl.consultarProduto(consulta);
								System.out.println("Informações do Produto");
								System.out.println("\tCódigo: " + produtoConsulta.getCodigo());
								System.out.println("\tNome: " + produtoConsulta.getNome());
								System.out.println("\tEdereço: " + produtoConsulta.getPreco());
								System.out.println("\tMarca: " + produtoConsulta.getMarca());
								System.out.println("\tCódigo do Fornecedor: "
										+ produtoConsulta.getFornecedor().getCodigoFornecedor());
							} else {
								System.out.println("Código do Produto não Existe");
							}
							break;
						case 6:
							consulta = 0;
							System.out.println("Consultar Venda");
							System.out.print("Código da Venda: ");
							consulta = Integer.parseInt(scan.nextLine());
							Venda vendaConsulta = new Venda();
							if (consulta == vendaDAOImpl.consultarVenda(consulta).getCodigoVenda()) {
								vendaConsulta = vendaDAOImpl.consultarVenda(consulta);
								System.out.println("Informações da Venda");
								System.out.println("\tCódigo da Venda: " + vendaConsulta.getCodigoVenda());

								System.out.println("Lista de Produtos");

								for (Produtos lista : vendaConsulta.getProdutos()) {

									produto = produtoDAOImpl.consultarProduto(lista.getCodigo());

									System.out.println("\nInformações do Produto");
									System.out.println("\tCódigo: " + produto.getCodigo());
									System.out.println("\tNome: " + produto.getNome());
									System.out.println("\tEdereço: " + produto.getPreco());
									System.out.println("\tMarca: " + produto.getMarca());
									System.out.println(
											"\tCódigo do Fornecedor: " + produto.getFornecedor().getCodigoFornecedor());
								}

								System.out.println("\tValor: " + vendaConsulta.getValor());
								System.out.println("\tData: " + vendaConsulta.getData());
								System.out.println("\tForma de Pagamento: " + venda.getFormadePagamento());

							} else {
								System.out.println("Código da Venda não Existe");
							}
							break;
						default:
							break;
						}
					} while (op != 7);

					logoff = "logoff";
					System.out.println("Bye");
				}
			} while (logoff != "logoff");

		} while (exit != "exit");
		scan.close();
	}

}
