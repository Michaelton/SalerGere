create database salergere;

use salergere;

create table usuario(
	tipoUsuario			varchar(250)	not null,
	login				varchar(250) 	not null 	primary key,
	senha 				varchar(250) 	not null
);

create table fornecedor(
	codigoFornecedor 	bigint 			not null 	primary key,
	nome 				varchar(250) 	not null,
	endereco 			varchar(250) 	not null,
	CNPJ 				bigint 			not null,
	email 				varchar(250) 	not null,
	telefone 			bigint 			not null
	
);

create table produto(
	codigo 				bigint	 		not null 	primary key,
    nome 				varchar(250) 	not null,
	preco 				double 			not null,
	marca 				varchar(250),
	codigoFornecedor 	bigint,
    
	foreign key(codigoFornecedor) references fornecedor(codigoFornecedor)
);

create table venda(
	codigoVenda 		bigint 			not null 	primary key		auto_increment,
    valor 				double 			not null,
    data 				date 			not null,
	formaPagamento 		varchar(250) 	not null
);

create table venda_produto(
codigo 					bigint,
codigoVenda 			bigint,

foreign key(codigo) references produto(codigo),
foreign key(codigoVenda) references venda(codigoVenda)
);

insert into usuario values('admin','admin','admin');
insert into usuario values('Usuário Nível 1','Usuário Teste','Senha Teste');

insert into fornecedor values(1,'Fornecedor1','Endereço1', 10123456789, 'email@fornecedor1', 11987654321);
insert into fornecedor values(2,'Fornecedor2','Endereço2', 20123456789, 'email@fornecedor2', 22987654321);
insert into fornecedor values(3,'Fornecedor3','Endereço3', 30123456789, 'email@fornecedor3', 33987654321);

insert into produto values(1,'Produto1', 12.34, 'Marca1', 1);
insert into produto values(2,'Produto2', 23.45, 'Marca2', 2);
insert into produto values(3,'Produto3', 34.50, 'Marca3', 3);

insert into venda values(1, 70.29, '2018-06-03', 'Débito');

insert into venda_produto values(1, 1);
insert into venda_produto values(2, 1);
insert into venda_produto values(3, 1);

